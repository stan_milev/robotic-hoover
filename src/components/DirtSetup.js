import React from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col,
    Button
} from 'react-bootstrap';

import Grid from './grid/Grid';

require('styles/DustSetup.scss');

class DustSetup extends React.Component {

  static propTypes = {
    actions: PropTypes.shape({
      setAppModelProperty: PropTypes.func.isRequired
    }).isRequired,
    rows: PropTypes.number.isRequired,
    cols: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      dirt: []
    };
  }

  toggleDirt(x, y) {
    const dirt = [...this.state.dirt];
    const idx = dirt.findIndex(d => d.x === x && d.y === y);
    if (idx === -1) {
      dirt.push({x, y});
    } else {
      dirt.splice(idx, 1);
    }

    this.setState({
      dirt
    });
  }

  renderGrid() {
    const rows = this.props.rows;
    const cols = this.props.cols;

    return (
      <Grid classes="setup-grid" rows={rows} cols={cols} dirt={this.state.dirt} onTileClick={(x, y) => this.toggleDirt(x, y)}/>
    );
  }

  render() {
    return (
      <Row className="row-setup">
        <Col xs={12} className="col-title">
          <h2>Step 2: Lets put some dirt</h2>
        </Col>
        <Col xs={12} className="col-grid">
          {this.renderGrid()}
        </Col>
        <Col xs={12} className="col-next">
          <Button
            onClick={() => this.props.actions.setAppModelProperty('dirt', this.state.dirt)}
            disabled={this.state.dirt.length === 0}
          >
            Next
          </Button>
        </Col>
      </Row>
    );
  }
}

export default DustSetup;

import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

require('styles/GridTile.scss');

class GridTile extends React.Component {

  static propTypes = {
    row: PropTypes.number.isRequired,
    col: PropTypes.number.isRequired,
    onTileClick: PropTypes.func.isRequired,
    classes: PropTypes.string,
    robot: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    })
  };

  static defaultProps = {
    onTileClick: () => false,
    robot: null
  };

  render() {
    const x = this.props.col;
    const y = this.props.row;
    const robot = JSON.stringify(this.props.robot) === JSON.stringify({x, y})
        ? <div className="robot"/>
        : null;

    const classes = cx('grid-tile', this.props.classes);
    return (
      <div
        id={`${x}-${y}`}
        className={classes}
        onClick={() => this.props.onTileClick(x, y)}
        role="button">
        {robot}
      </div>
    );
  }
}

export default GridTile;

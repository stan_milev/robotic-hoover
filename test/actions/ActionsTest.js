import * as ActionTypes from '../../src/constants/ActionTypes';

import moveRobot from '../../src/actions/moveRobot';
import setAppModelPropery from '../../src/actions/setAppModelPropery';
import resetAppModel from '../../src/actions/resetAppModel';


describe('App actions', () => {

  describe('#setAppModelPropery', () => {

    it('should create an action to set property to the AppModel', () => {

      const propertyKey = 'robot';
      const propertyValue = {
        position: {
          x: 0,
          y: 0
        }
      };

      const expectedAction = {
        type: ActionTypes.SET_APP_MODEL_PROPERTY,
        propertyKey,
        propertyValue,
      };

      expect(setAppModelPropery(propertyKey, propertyValue), 'setAppModelPropery action').to.eql(expectedAction);
    });
  });

  describe('#resetAppModel', () => {

    it('should create an action to reset the AppModel', () => {

      const expectedAction = {
        type: ActionTypes.RESET_APP_MODEL,
      };

      expect(resetAppModel(), 'resetAppModel action').to.eql(expectedAction);
    });
  });

  describe('#moveRobot', () => {

    it('should create an action to change the robot position', () => {

      const dX = 1;
      const dY = 0;

      const expectedAction = {
        type: ActionTypes.MOVE_ROBOT,
        dX,
        dY,
      };

      expect(moveRobot(dX, dY), 'moveRobot action').to.eql(expectedAction);
    });
  });

});


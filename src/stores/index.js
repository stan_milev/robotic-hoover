/* eslint-disable import/no-extraneous-dependencies */
import { createStore, applyMiddleware, compose } from 'redux';
import Config from 'config';
import reducers from '../reducers';
import logActionAndState from '../middleware/logging';

function reduxStore(initialState) {
  // add any middlewares here
  const middlewares = [
    Config.enableLogging ? logActionAndState : null
  ];


  const store = createStore(reducers, initialState,
      compose(
        // pass only middlewares that are not null
        applyMiddleware(...middlewares.filter(n => n)),
        window.devToolsExtension && window.devToolsExtension()
      )
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      // We need to require for hot reloading to work properly.
      const nextReducer = require('../reducers');  // eslint-disable-line global-require

      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

export default reduxStore;

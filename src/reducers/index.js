import { combineReducers } from 'redux-immutable';
import appModel from './appModel';

const reducers = { appModel };
const combined = combineReducers(reducers);
module.exports = combined;

import React from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col,
    Button
} from 'react-bootstrap';

import Grid from './grid/Grid';
import * as KeyCode from '../constants/KeyCode';

require('styles/Game.scss');

class Game extends React.Component {

  static propTypes = {
    actions: PropTypes.shape({
      resetAppModel: PropTypes.func.isRequired,
      moveRobot: PropTypes.func.isRequired
    }).isRequired,
    rows: PropTypes.number.isRequired,
    cols: PropTypes.number.isRequired,
    robot: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }).isRequired,
    dirt: PropTypes.arrayOf(
      PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
      })
    ).isRequired
  };

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }
  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown(e) {
    if (e.code === KeyCode.ARROW_UP) {
      this.props.actions.moveRobot(0, 1);
    }
    if (e.code === KeyCode.ARROW_DOWN) {
      this.props.actions.moveRobot(0, -1);
    }
    if (e.code === KeyCode.ARROW_LEFT) {
      this.props.actions.moveRobot(-1, 0);
    }
    if (e.code === KeyCode.ARROW_RIGHT) {
      this.props.actions.moveRobot(1, 0);
    }
  }

  renderGrid() {
    const rows = this.props.rows;
    const cols = this.props.cols;

    return (
      <Grid
        classes="game-grid"
        rows={rows}
        cols={cols}
        dirt={this.props.dirt}
        robot={this.props.robot}
      />
    );
  }

  render() {
    return (
      // eslint-disable-next-line no-return-assign
      <Row className="row-game" ref={node => this.node = node}>
        <Col xs={12} className="col-title">
          <h2>Lets clean this mess</h2>
          <p>Use the <strong>W, S, A, D</strong> keys to move UP, DOWN, LEFT and RIGHT</p>
        </Col>
        <Col xs={12} className="col-grid">
          {this.renderGrid()}
        </Col>
        <Col xs={12} className="col-next">
          <Button
            onClick={() => this.props.actions.resetAppModel()}
          >
            Reset
          </Button>
        </Col>
      </Row>
    );
  }
}

export default Game;

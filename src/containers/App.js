import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import Main from '../components/Main';
import setAppModelProperty from '../actions/setAppModelPropery';
import resetAppModel from '../actions/resetAppModel';
import moveRobot from '../actions/moveRobot';

class App extends Component {
  render() {
    const { actions, appModel } = this.props;
    return <Main actions={actions} appModel={appModel} />;
  }
}

App.propTypes = {
  actions: PropTypes.shape({
    setAppModelProperty: PropTypes.func.isRequired,
    resetAppModel: PropTypes.func.isRequired,
    moveRobot: PropTypes.func.isRequired
  }).isRequired,
  appModel: PropTypes.instanceOf(Immutable.Map).isRequired,
};

function mapStateToProps(state) { // eslint-disable-line no-unused-vars
  const props = {
    appModel: state.get('appModel'),
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    setAppModelProperty,
    resetAppModel,
    moveRobot
  };
  const actionMap = {
    actions: bindActionCreators(actions, dispatch)
  };
  return actionMap;
}
export default connect(mapStateToProps, mapDispatchToProps)(App);

/* eslint-disable linebreak-style */
import Immutable from 'immutable';
import * as ActionTypes from '../constants/ActionTypes';

const initialState = Immutable.fromJS({
  room: {
    rows: 0,
    cols: 0
  },
  robot: {},
  dirt: []
});

module.exports = function (state = initialState, action) {

  switch (action.type) {
    case ActionTypes.SET_APP_MODEL_PROPERTY: {
        // if setting the robot position set the property
        // and remove any items from dirt with the same position
      if (action.propertyKey === 'robot') {
        const position = Immutable.fromJS(action.propertyValue.position);
        const dirt = state.get('dirt').filter(tile => !tile.equals(position));
        const newState = state.set('dirt', dirt);
        return newState.setIn(['robot', 'position'], position);
      }
        // set the state property
      return state.set(action.propertyKey, Immutable.fromJS(action.propertyValue));
    }
    case ActionTypes.RESET_APP_MODEL:
      // restore the initial state
      return state.merge(initialState);

    case ActionTypes.MOVE_ROBOT: {

      const currentX = state.getIn(['robot', 'position', 'x']);
      const currentY = state.getIn(['robot', 'position', 'y']);
      const newX = currentX + action.dX;
      const newY = currentY + action.dY;
      const range = new Immutable.Range(0, state.getIn(['room', 'rows']), 1);

        // set the new position only if it is within the boundaries of the room
      if (range.includes(newX) && range.includes(newY)) {
        const newPosition = Immutable.fromJS({x: newX, y: newY});
            // remove any item from dirt that matches the new position
        const dirt = state.get('dirt').filter(tile => !tile.equals(newPosition));
        const newState = state.set('dirt', dirt);
        return newState.mergeDeepIn(['robot', 'position'], newPosition);
      }
        // if trying to move outside do nothing
      return state;
    }
    default:
      return state;

  }
};

import React from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col,
    Button
} from 'react-bootstrap';

import Grid from './grid/Grid';

require('styles/RobotPositionSetup.scss');

class RobotPositionSetup extends React.Component {

  static propTypes = {
    actions: PropTypes.shape({
      setAppModelProperty: PropTypes.func.isRequired
    }).isRequired,
    rows: PropTypes.number.isRequired,
    cols: PropTypes.number.isRequired,
    dirt: PropTypes.arrayOf(
      PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
      })
    ).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      position: null
    };
  }

  addRobot(x, y) {
    this.setState({
      position: {x, y}
    });
  }

  renderGrid() {
    const rows = this.props.rows;
    const cols = this.props.cols;

    return (
      <Grid
        classes="setup-grid"
        rows={rows}
        cols={cols}
        dirt={this.props.dirt}
        robot={this.state.position}
        onTileClick={(x, y) => this.addRobot(x, y)}
      />
    );
  }

  render() {
    return (
      <Row className="row-setup">
        <Col xs={12} className="col-title">
          <h2>Step 3: Place the robotic hoover</h2>
        </Col>
        <Col xs={12} className="col-grid">
          {this.renderGrid()}
        </Col>
        <Col xs={12} className="col-next">
          <Button
            onClick={() => this.props.actions.setAppModelProperty('robot', {position: this.state.position})}
            disabled={this.state.position === null}
          >
            Next
          </Button>
        </Col>
      </Row>
    );
  }
}

export default RobotPositionSetup;

import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class GridRow extends React.Component {

  static propTypes = {
    children: PropTypes.node.isRequired,
    row: PropTypes.number.isRequired,
    classes: PropTypes.string
  };

  render() {

    const classes = cx('grid-row', this.props.classes);
    return (
      <div
        id={this.props.row}
        key={this.props.row}
        className={classes}>
        {this.props.children}
      </div>
    );
  }
}

export default GridRow;

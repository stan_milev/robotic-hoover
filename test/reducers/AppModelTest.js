import Immutable from 'immutable';
import * as ActionTypes from '../../src/constants/ActionTypes';

const reducer = require('../../src/reducers/appModel');

describe('appModel', () => {

  const initialState = Immutable.fromJS({
    room: {
      rows: 0,
      cols: 0
    },
    robot: {},
    dirt: []
  });

  const roomSetupAction = {
    type: ActionTypes.SET_APP_MODEL_PROPERTY,
    propertyKey: 'room',
    propertyValue: {
      rows: 5,
      cols: 5
    }

  };

  const dirtArr = [
        {x: 0, y: 0},
        {x: 1, y: 1},
        {x: 4, y: 3},
        {x: 5, y: 5},
  ];

  const dirtSetupAction = {
    type: ActionTypes.SET_APP_MODEL_PROPERTY,
    propertyKey: 'dirt',
    propertyValue: dirtArr
  };

  const robotPositionAction = {
    type: ActionTypes.SET_APP_MODEL_PROPERTY,
    propertyKey: 'robot',
    propertyValue: { position: { x: 0, y: 0 } }
  };

  const moveRightAction = {
    type: ActionTypes.MOVE_ROBOT,
    dX: 1,
    dY: 0
  };

  const moveUpAction = {
    type: ActionTypes.MOVE_ROBOT,
    dX: 0,
    dY: 1
  };

  const moveDownAction = {
    type: ActionTypes.MOVE_ROBOT,
    dX: 0,
    dY: -1
  };

  const moveLeftAction = {
    type: ActionTypes.MOVE_ROBOT,
    dX: -1,
    dY: 0
  };

  const resetAppModelAction = {
    type: ActionTypes.RESET_APP_MODEL
  };

  const roomSetupState = reducer(initialState, roomSetupAction);
  const dirtSetupState = reducer(roomSetupState, dirtSetupAction);
  const robotSetupState = reducer(dirtSetupState, robotPositionAction);
  const movedRightState = reducer(robotSetupState, moveRightAction);
  const movedUpState = reducer(movedRightState, moveUpAction);
  const movedDownState = reducer(movedUpState, moveDownAction);
  const movedLeftState = reducer(movedDownState, moveLeftAction);
  const attemptMoveLeftState = reducer(movedLeftState, moveLeftAction);
  const resetAppModelState = reducer(attemptMoveLeftState, resetAppModelAction);

  describe('#default', () => {
    it('should return the initial state', () => {

      const newState = reducer(initialState, {
        type: 'INVALID'
      });

      expect(newState, 'newState').to.eql(initialState);
    });
  });

  describe('#SET_APP_MODEL_PROPERTY', () => {
    describe('#SET_APP_MODEL_PROPERTY room:{rows:5, cols:5}', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(roomSetupState, 'roomSetupState').to.not.eql(initialState);
      });

      it('should set the room dimentions', () => {
        const room = roomSetupState.get('room').toJS();
        // the new state should have a propery room with a value {rows:5, cols:5}
        expect(room, 'room').to.eql({rows: 5, cols: 5});
      });
    });

    describe('#SET_APP_MODEL_PROPERTY dirt:[{x:0, y:0},...]', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(dirtSetupState, 'dirtSetupState').to.not.eql(roomSetupState);
      });

      it('should set the dirt positions', () => {
        const dirt = dirtSetupState.get('dirt').toJS();
        // the new state should have a property dirt that is an array with 4 items
        // and is equal to the array used in the action
        expect(dirt, 'dirt').to.be.an('array')
          .that.has.length(4)
          .and.is.eql(dirtArr);
      });
    });

    describe('#SET_APP_MODEL_PROPERTY robot:{ position: {x:0, y:0} }', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(robotSetupState, 'robotSetupState').to.not.eql(dirtSetupState);
      });

      it('should set the robot position', () => {
        // the robot should have a property position equal to {x:0, y:0}
        expect(robotSetupState.get('robot').toJS(), 'robot').to.have.property('position')
          .that.is.eql({x: 0, y: 0});
      });

      it('should remove any dirt item if at the robot position', () => {
        // this should remove the first item of the dirt array
        expect(robotSetupState.get('dirt').toJS(), 'dirt').to.have.length(3)
          .and.to.not.include({x: 0, y: 0});
      });
    });
  });

  describe('#MOVE_ROBOT', () => {
    describe('#RIGHT', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(movedRightState, 'movedRightState').to.not.eql(robotSetupState);
      });

      it('should set the robot position', () => {
        // the new robot position should be {x:1, y:0}
        expect(movedRightState.getIn(['robot', 'position']).toJS(), 'robot.position').to.eql({x: 1, y: 0});
      });

      it('should not modify dirt array', () => {
        // this should not modify the dirt array
        expect(movedRightState.get('dirt').toJS(), 'dirt').to.have.length(3)
          .and.to.have.deep.members([{x: 1, y: 1}, {x: 4, y: 3}, {x: 5, y: 5}]);
      });
    });

    describe('#UP', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(movedUpState, 'movedUpState').to.not.eql(movedRightState);
      });

      it('should set the robot position', () => {
        // the new robot position should be {x:1, y:1}
        expect(movedUpState.getIn(['robot', 'position']).toJS(), 'robot.position').to.eql({x: 1, y: 1});
      });

      it('should remove any dirt item if at the robot position', () => {
        // this should remove the first item of the dirt array
        expect(movedUpState.get('dirt').toJS(), 'dirt').to.have.length(2)
          .and.to.not.include({x: 1, y: 1});
      });
    });

    describe('#DOWN', () => {
      it('should not return the same state', () => {
              // this should return a new state
        expect(movedDownState, 'movedDownState').to.not.eql(movedUpState);
      });

      it('should set the robot position', () => {
        // the new robot position should be {x:1, y:0}
        expect(movedDownState.getIn(['robot', 'position']).toJS(), 'robot.position').to.eql({x: 1, y: 0});
      });
    });

    describe('#LEFT', () => {
      it('should not return the same state', () => {
        // this should return a new state
        expect(movedLeftState, 'movedLeftState').to.not.eql(movedRightState);
      });

      it('should set the robot position', () => {
        // the new robot position should be {x:1, y:0}
        expect(movedLeftState.getIn(['robot', 'position']).toJS(), 'robot.position').to.eql({x: 0, y: 0});
      });
    });

    describe('#ATTEMPT LEFT', () => {
      it('should return the same state', () => {
        // attempting to move left of {x:0, y0} should return unchanged state
        expect(attemptMoveLeftState, 'attemptMoveLeftState').to.eql(movedLeftState);
      });
    });
  });

  describe('#RESET_APP_MODEL', () => {
    it('should return the initial state', () => {
      expect(resetAppModelState, 'resetAppModelState').to.eql(initialState);
    });
  });
});

/**
 * Logs all actions and states after they are dispatched.
 */
const logActionAndState = store => next => (action) => {
  console.group(action.type);
  console.info('dispatching', action);

  const result = next(action);

  console.log('next state', store.getState().toJS());
  console.groupEnd(action.type);

  return result;
};

export default logActionAndState;

# README #

tray.io Front End Technical Test - Robotic Hoover

### Setup ###
git clone https://bitbucket.org/stan_milev/robotic-hoover.git  
cd robotic-hoover  
npm install  


### Run ###
npm start


### Test 
npm test  
npm test:watch  
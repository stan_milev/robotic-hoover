import baseConfig from './base';

const config = {
  appEnv: 'dev',
  enableLogging: true
};

export default Object.freeze(Object.assign({}, baseConfig, config));

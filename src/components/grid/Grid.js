import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import GridRow from './GridRow';
import GridTile from './GridTile';

require('styles/Grid.scss');

class Grid extends React.Component {

  static propTypes = {
    rows: PropTypes.number.isRequired,
    cols: PropTypes.number.isRequired,
    classes: PropTypes.string.isRequired,
    dirt: PropTypes.arrayOf(
      PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
      })
    ),
    robot: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }),
    onTileClick: PropTypes.func,
  };

  static defaultProps = {
    onTileClick: () => false,
    dirt: [],
    robot: null
  };

  render() {
    const classes = cx('grid', this.props.classes);
    const rows = this.props.rows;
    const cols = this.props.cols;
    const dirt = this.props.dirt;

    const grid = [...Array(rows)].map((r, y) => (
      <GridRow key={y} row={y}>
        {[...Array(cols)].map((c, x) =>
          <GridTile
            key={x}
            row={y}
            col={x}
            robot={this.props.robot}
            classes={cx({ dirty: dirt.find(d => d.x === x && d.y === y) })}
            onTileClick={(...args) => this.props.onTileClick(...args)}
           />
        )}
      </GridRow>
      )).reverse();

    return (
      <div className={classes}>
        {grid}
      </div>
    );
  }
}

export default Grid;

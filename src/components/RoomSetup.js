import React from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col,
    ButtonGroup,
    Button
} from 'react-bootstrap';

require('styles/RoomSetup.scss');

class RoomSetup extends React.Component {

  static propTypes = {
    actions: PropTypes.shape({
      setAppModelProperty: PropTypes.func.isRequired
    }).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      size: 3
    };
  }

  render() {
    return (
      <Row className="row-setup">
        <Col xs={12} className="col-title">
          <h2>Step 1: Set up room size</h2>
        </Col>
        <Col xs={12} className="col-controls">

          <ButtonGroup className="room-controls">
            <Button
              onClick={() => this.setState({
                size: this.state.size > 3
                    ? this.state.size - 1
                    : this.state.size})
                  }
            >
              -
            </Button>
            <Button disabled>
              <strong>{this.state.size}</strong> by <strong>{this.state.size}</strong>
            </Button>
            <Button
              onClick={() => this.setState({
                size: this.state.size < 10
                  ? this.state.size + 1
                  : this.state.size})
              }
            >
              +
            </Button>
          </ButtonGroup>
        </Col>
        <Col xs={12} className="col-next">
          <Button
            onClick={() => this.props.actions.setAppModelProperty('room', {
              rows: this.state.size,
              cols: this.state.size
            })}
          >
            Next
          </Button>
        </Col>
      </Row>
    );
  }
}

export default RoomSetup;

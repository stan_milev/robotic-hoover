import * as ActionTypes from '../constants/ActionTypes';

module.exports = function (dX, dY) {
  return {
    type: ActionTypes.MOVE_ROBOT,
    dX,
    dY
  };
};


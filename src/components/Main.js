import React from 'react';
import PropTypes from 'prop-types';
import {
    Row,
    Col
} from 'react-bootstrap';
import Immutable from 'immutable';

import RoomSetup from './RoomSetup';
import DirtSetup from './DirtSetup';
import RobotPositionSetup from './RobotPositionSetup';
import GameView from './Game';

require('styles/Main.scss');

class AppComponent extends React.Component {
  static propTypes = {
    appModel: PropTypes.instanceOf(Immutable.Map).isRequired,
    actions: PropTypes.shape({}).isRequired
  };

  render() {
    const actions = this.props.actions;
    const rows = this.props.appModel.getIn(['room', 'rows']);
    const cols = this.props.appModel.getIn(['room', 'cols']);
    const dirt = this.props.appModel.get('dirt');
    const robotPosition = this.props.appModel.getIn(['robot', 'position']);

    const isRoomSetup = rows > 0;
    const isDirtSetup = dirt.size > 0;

    const roomSetup = !isRoomSetup
      ? <RoomSetup actions={actions} />
      : null;

    const dirtSetup = isRoomSetup && !isDirtSetup && !robotPosition
      ? <DirtSetup actions={actions} rows={rows} cols={cols} />
      : null;

    const robotPositionSetup = isRoomSetup && isDirtSetup && !robotPosition
      ? (<RobotPositionSetup actions={actions} rows={rows} cols={cols} dirt={dirt.toJS()}/>)
      : null;

    const gameView = robotPosition
      ? <GameView actions={actions} rows={rows} cols={cols} dirt={dirt.toJS()} robot={robotPosition.toJS()} />
      : null;

    return (
      <div className="container-fluid">
        <Row>
          <Col>
            <h1>Robotic Hoover</h1>
          </Col>
        </Row>
        {roomSetup}
        {dirtSetup}
        {robotPositionSetup}
        {gameView}
      </div>
    );
  }
}

export default AppComponent;

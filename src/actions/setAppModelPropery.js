import * as ActionTypes from '../constants/ActionTypes';

module.exports = function (key, value) {
  return {
    type: ActionTypes.SET_APP_MODEL_PROPERTY,
    propertyKey: key,
    propertyValue: value,
  };
};

